---
layout: markdown_page
title: "Category Vision - Container Registry"
---

- TOC
{:toc}

## Container Registry

The GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, the GitLab Container Registry is completely integrated with GitLab.  Easily use your images with GitLab CI, create images specific for tags or branches and much more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContainer+Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

Our primary focus is on improving the performance and experience of untagging and deleting images from the Container Registry. We are prioritizing this work so that we can help our customers to lower the cost of storage for the Container Registry, improve the performance of the application and reduce the risk inherent to manually deleting images. 

Our highest priority issue is [gitlab-#31071](https://gitlab.com/gitlab-org/gitlab/issues/31071), which will seek to optimize the Container Registry garbage collection algorithm. The goal of this issue will be to unblock our customers from deleting images from storage by decreasing the amount of down/read-only time required to run garbage collection. It's important to note, that this will not enable online garbage collection.  [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will allow the process to run without requiring the Container Registry to be in read-only mode or to schedule down time. 

We have also prioritized improving the tag removal process, which defines which images can be deleted as part of garbage collection. [gitlab-#31832](https://gitlab.com/gitlab-org/gitlab/issues/31832) will improve the performance of the tag removal API that powers the front end of the Container Registry. [gitlab-#15398](https://gitlab.com/gitlab-org/gitlab/issues/15398) will introduce retention and expiration policies that will allow users to specify, using regex, which images they would like to keep and expire. There are some open questions we are working through in this issue, such as, should policies be enforced at the instance or project level. Please take a look at the issue and join the conversation. 

Finally, we are investigating [gitlab-#34073](https://gitlab.com/gitlab-org/gitlab/issues/34073). The goal of this issue will be to allow users to delete branch images once their merge request has been accepted, similar to how they can choose to delete their branch or squash commits on merge.

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
- [Improve the performance and reliability of garbage collection](https://gitlab.com/gitlab-org/gitlab-ce/issues/66347)
- [Storage tracking and limits for the Container Registry](https://gitlab.com/gitlab-org/gitlab-ce/issues/59232)
- [Implement in-line garbage collection](https://gitlab.com/gitlab-org/gitlab-ce/issues/57897)
- [Retention / expiration policy](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247)
- [Fix Tag pruning and deletion logic](https://gitlab.com/gitlab-org/gitlab-ce/issues/21405) (Complete)
- [Sort images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/20216)
- [Filter images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/62309)

## Competitive Landscape

[JFrog](https://jfrog.com/artifactory/) and [Sonatype](https://www.sonatype.com/nexus-repository-sonatype) both offer support for building and deployin Docker images. GitHub has a product in [beta](https://help.github.com/en/articles/configuring-docker-for-use-with-github-package-registry) that allows users to authenticate, publish and install packages utilizing Docker. 

Container registries such as [Docker Hub](https://hub.docker.com/) and [Quay](https://quay.io/) offer users a single location to build, analyze and distribute their container images. 

GitLab provides an improved experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a Package Management tool, but without the weight and complexity of a single-point solution. We will prioritize security, performance and integration without sacrificing user experience. 

## Top Customer Success/Sales Issue(s)

The top Customer Success / Sales issue is to improve the visibility and management layer of the Container Registry. The goal of [gitlab-ce#29639](https://gitlab.com/gitlab-org/gitlab-ce/issues/29639) is to improve the tracking and display of data to provide a more seamless user experience within GitLab. By completing this issue we will:
-  Allow  metadata to be stored and removed
-  Make it possible to easily track what data is stored in the registry
-  Make it possible to introduce retention policies for images stored in the registry


## Top Customer Issue(s)

The top customer issue is [gitlab-#31071](https://gitlab.com/gitlab-org/gitlab/issues/31071) which will unblock customers from running garbage collection. [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will remove the requirement for down time and unblock all of our customers (and GitLb) from running garbage collection. 

There are additional top TAM issues identified which are popular amongst our customers:

- [GitLab Registry available images list/search](https://gitlab.com/gitlab-org/gitlab-ce/issues/26866)
- [Retention/expiration policy for container images](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247)

## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing in-line garbage collection and removal of blobs. 

## Top Vision Item(s)

We've learned from a recent [survey](https://gitlab.com/gitlab-org/ux-research/issues/328) and subsequent [user interviews](https://gitlab.com/gitlab-org/ux-research/issues/329), that users navigate to the Container Registry user interface for [one of three reasons](https://gitlab.com/gitlab-org/uxr_insights/issues/617).

- To look up which image or tag I should be using in my environment (32%)
- To verify my CI pipeline built the image as expected	(28%)
- To ensure my image was uploaded correctly (22%)

Our top vision item, [gitlab-#31636](https://gitlab.com/gitlab-org/gitlab/issues/31636) aims to add the required metadata and information to help users accomplish those tasks. 